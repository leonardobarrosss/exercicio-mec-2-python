# Resolve problema de Mecanica (5-205)

import math
from matplotlib import pyplot

print ("Pensando no cursor e na manivela do problema anterior, desenvolva a expressão para a aceleração αA do pistão (admitindo positiva para a direita) como uma função de θ para w = θ = constante. Substitua dados numéricos: l = 350 mm com o centro de massa G a 100 mm de B, r = 125 mm e calcule αA, contra θ e encontre o valor de θ para 0 ≤ θ ≤ 180º. Represente graficamente αA, contra θ e encontre o valor de θ para o qual αA = 0. (Usando simetria, faça uma previsão dos resultados para, 180º ≤ θ ≤ 360º.)")

# Velocidade angular obtida no exercicio anterior
w = (157.1)

l = float(input('Insira a distancia do braço da manivela (l) em metros: '))

r = float(input('Insira a distancia do raio em metros (r) : '))
    
teta = range(361)

aceA = [((r)*(w**2))*(math.cos(i*math.pi/180.0)+(r/l)*(((1-2*(math.sin(i*math.pi/180.0))**2)+((r**2)/(l**2))*((math.sin(i*math.pi/180.0))**4))/((1-((r**2)/(l**2))*(math.sin(i*math.pi/180.0)**2))**(3/2)))) for i in teta]

pyplot.plot(teta, aceA)

pyplot.grid(True)

pyplot.xlabel(r'$\theta\,(^{o})$')

pyplot.ylabel("Aceleração (m/s²)")

pyplot.savefig('grafico.png')
    
pyplot.show()
